import sys

#Usage function
def usage():
    print "Usage: python loomtask.py <file>"

if __name__ == "__main__":
    #check for input file
    if len(sys.argv) < 2:
        usage()
    #open the input file for reading and the output file for writing
    inputFile = open(sys.argv[1], "r")
    outputFile = open("results.txt", "w+")
    #write 'Results:' at the top of the output file
    outputFile.write("Results:\n")
    #make the input file data into a list
    lines = list(inputFile)
    wordList = []
    #split each string into its the date, time, and words. Take only the words
    #(e.g.) '02-12-2012 Naomi is getting into the car' -> ['Naomi', 'is', 'getting', 'into', 'the', 'car']
    for line in lines:
        wordList.append(line.split()[2:])
    #get length of list
    numLines = len(wordList)
    for x in range(0, numLines):
        for y in range(x, numLines):
            #if length of strings are different, go to next comparison
            if len(wordList[x]) != len(wordList[y]):
                continue
            #setup variables for counter and changing word
            count = 0
            prevWord = ""
            postWord = ""
            #compare each word between the two strings
            for index, word in enumerate(wordList[y]):
                #if the word between strings is different, then add 1 to count
                if word != wordList[x][index]:
                    #if you find more than 1 difference, you are done, break out of loop
                    if count > 1:
                        break
                    #else add 1 to count, keep track of changing word
                    count = count + 1
                    prevWord = wordList[x][index]
                    postWord = word
            #if you find 1 total difference, output the original lines and the changing
            #to the output file
            if count == 1:
                outputFile.write(lines[x])
                outputFile.write(lines[y])
                outputFile.write("The changing word was: " + prevWord + ", " + postWord + "\n")
                outputFile.write("\n")
    #close I/O files
    inputFile.close()
    outputFile.close()
